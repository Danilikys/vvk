

jQuery(function($) {

  //DIAGRAM ON

  if ($('.top-text__diagram').length){

    $('.top-text__diagram-item__count span').each(function(){
      var tmpCount = $(this).text();
      var tmpPercent = 452 - (452 * tmpCount / 100);

      $(this).closest('.top-text__diagram-item').find('.circle_animation').css('stroke-dashoffset', tmpPercent)
    });

  }

  //DIAGRAM OFF

  //AKKOTABS ON

  function akkotabs() {

    if ($('.repaired').length){

      if (window.matchMedia("(min-width: 1200px)").matches) {

        // Верхнее меню

        var arrTitles = [];
        var tmpTitles;

        $('.repaired__item-title').each(function(){

          tmpTitles = $(this).text();
          arrTitles.push(tmpTitles);
          $(this).remove()

        });

        var index, len;
        for (index = 0, len = arrTitles.length; index < len; ++index) {
          $(".repaired__main-menu").append("<div class=\"repaired__main-menu__item\">" + arrTitles[index] + "</div>");
        }

        $('.repaired__main-menu__item').eq(0).addClass('active');

        // Боковое меню

        $('.repaired__item-list').each(function(){

          var arrSubTitles = [];
          var tmpSubTitles;

          $(this).find('.repaired__item-list__item-title').each(function(){

            tmpSubTitles = $(this).text();

            arrSubTitles.push(tmpSubTitles);
            $(this).remove()

          });

          var subIndex, sublen;
          for (subIndex = 0, sublen = arrSubTitles.length; subIndex < sublen; ++subIndex) {
            $(this).find(".repaired__item-list__menu").append("<div class=\"repaired__item-list__menu-item\">" + arrSubTitles[subIndex] + "</div>");
          }

          $('.repaired__item-list__menu-item').eq(0).addClass('active');

          arrSubTitles = [];

        });

        // Собственно табы

        var tmpMainMenu;

        $('.repaired__main-menu__item').on('click', function () {

          if ($(this).hasClass('active')){

            tmpMainMenu = $(this).index();

            console.log(tmpMainMenu);

            $('.repaired__item').removeClass('active')
            $('.repaired__item').eq(tmpMainMenu).addClass('active')

          } else {

            $('.repaired__main-menu__item').removeClass('active');
            $(this).addClass('active');

            tmpMainMenu = $(this).index();

            $('.repaired__item').removeClass('active');
            $('.repaired__item').eq(tmpMainMenu).addClass('active');

            $('.repaired__item').eq(tmpMainMenu).find('.repaired__item-list__menu-item').removeClass('active');
            $('.repaired__item').eq(tmpMainMenu).find('.repaired__item-list__menu-item').eq(0).addClass('active');

            $('.repaired__item').eq(tmpMainMenu).find('.repaired__item-list__item').removeClass('active');
            $('.repaired__item').eq(tmpMainMenu).find('.repaired__item-list__item').eq(0).addClass('active');

            $('.repaired__bot-middle__block').removeClass('active');
            $('.repaired__bot-middle__block').eq(tmpMainMenu).addClass('active');
          }
        })

        // Собственно вложенные табы

        $('.repaired__item-list__menu-item').on('click', function () {

          if ($(this).hasClass('active')){

          } else {

            $('.repaired__item-list__menu-item').removeClass('active');
            $(this).addClass('active');

            var tmpSubMenu = $(this).index();

            $(this).closest('.repaired__item-list').find('.repaired__item-list__item').removeClass('active');
            $(this).closest('.repaired__item-list').find('.repaired__item-list__item').eq(tmpSubMenu).addClass('active');

          }

        });


      } else {

        // Собственно сам аккордеон

        $('.repaired__item-title').on('click', function () {

          if ($(this).closest('.repaired__item').hasClass('active')){

            $(this).closest('.repaired__item').removeClass('active');
            $(this).closest('.repaired__item').find('.repaired__item-list__item').removeClass('active');

          } else {

            var tmpEq =  $(this).closest('.repaired__item').index();
            $('.repaired__bot-middle__block').removeClass('active');
            $('.repaired__bot-middle__block').eq(tmpEq - 1).addClass('active');


            $('.repaired__item').removeClass('active');
            $('.repaired__item-list__item').removeClass('active');
            $(this).closest('.repaired__item').addClass('active');
            $(this).closest('.repaired__item').find('.repaired__item-list__item').eq(0).addClass('active');


          }


        });

        //  Собственно вложенный аккордеон

        $('.repaired__item-list__item-title').on('click', function () {

          if ($(this).closest('.repaired__item-list__item').hasClass('active')){

            $(this).closest('.repaired__item-list__item').removeClass('active');

          } else {

            $('.repaired__item-list__item').removeClass('active');

            $(this).closest('.repaired__item-list__item').addClass('active');

          }

        })
      }

    }

  }

  akkotabs();

   //AKKOTABS OFF

  // BURGER WORK ON

  $('.header-burger').on('click', function () {

    $('body').toggleClass('burger-opened');
    $('.header-bot').fadeToggle();

  });

  // BURGER WORK OFF

  // SLIDERS ON

  $(".first-slider").owlCarousel({
    loop: true,
    items: 1,
    margin: 0,
    dots: true,
    nav: true,
    navText: '',
    autoplay: true,
    autoplayTimeout: 10000
  });

 function forSliderAdv() {

   $(".slider-adv").owlCarousel({
     loop: true,
     items: 1,
     margin: 0,
     dots: true,
     nav: false,
     navText: '',
     autoplay: false,
     autoplayTimeout: 7000,
     onChanged: counter,
     responsiveClass:true,
     responsive:{
       0:{
         items: 1,
         margin: 0
       },
       768:{
         items: 2,
         margin: 15
       },
       1200:{
         items: 4,
         margin: 15
       }
     }
   });

  }

  function forSliderClients(){
    $(".clients__slider").owlCarousel({
      loop: true,
      items: 1,
      margin: 0,
      dots: true,
      nav: false,
      navText: '',
      autoplay:false,
      onChanged: counterClients,
      autoplayTimeout: 7000,
      responsiveClass:true,
      responsive:{
        0:{
          items: 1,
          margin: 0
        },
        768:{
          items: 3,
          margin: 15
        },
        1200:{
          items: 6,
          margin: 20
        }
      }
    });
  }

  forSliderAdv();
  forSliderClients();

  $('.slider-adv .owl-item.active:first').addClass('adv-current');

  function counter(event) {
    if (!event.namespace) {
      return;
    }
    var slides = event.relatedTarget;
    var countSlides;
    var currentSlide;
    var classCurrentSlide;
    var ObrclassCurrentSlide;

    countSlides = slides.items().length;
    currentSlide = slides.relative(slides.current()) + 1;
    classCurrentSlide = 'slide' + currentSlide;

    console.log(classCurrentSlide);

    if (classCurrentSlide == 'slideNaN'){

    } else{

      var currentSlideItem = '.' + classCurrentSlide + ' .item__subtext';

      // console.log(currentSlideItem);

      var textCurrentItem = $('body').find(currentSlideItem).eq(0).text();

      // console.log($('body').find(currentSlideItem).eq(0).text());

      $('.sl-adv .def-ask__title').text(textCurrentItem);

      $('.slider-adv .owl-item').removeClass('adv-current');
      $('.' + classCurrentSlide).closest('.owl-item').addClass('adv-current');

    }
  }

  function counterClients(event) {
    if (!event.namespace) {
      return;
    }
    var slides = event.relatedTarget;
    var countSlides;
    var currentSlide;
    var classCurrentSlide;

    countSlides = slides.items().length;
    currentSlide = slides.relative(slides.current()) + 1;
    classCurrentSlide = 'client' + currentSlide;

    // console.log(classCurrentSlide);

    if (classCurrentSlide == 'slideNaN'){

    } else{

      var currentSlideItemTitle = '.' + classCurrentSlide + ' .item__title';
      var currentSlideItemText = '.' + classCurrentSlide + ' .item__text';

      var textCurrentItemTitle = $('body').find(currentSlideItemTitle).text();
      var textCurrentItemText = $('body').find(currentSlideItemText).text();

      $('.clients .clients__monial-title').text(textCurrentItemTitle);
      $('.clients .clients__monial-text p').text(textCurrentItemText);

    }
  }

  $(window).resize(function() {
    forSliderAdv();
    forSliderClients();
  });

  // SLIDERS OFF

  // DOP SIDE MENU ON

  var activeClassName = 'active';
  var catalog = $('.catalogs-nav');

  catalog.on('click', 'span', function () {
    var _t = $(this);
    var _ta = _t.hasClass(activeClassName);

    if (_ta) {
      _t.removeClass(activeClassName);
    } else {
      _t.closest('ul').find('.' + activeClassName).removeClass(activeClassName);
      _t.addClass(activeClassName);
    }
  });

  // DOP SIDE MENU OFF

  //  FORM ON

        // DEFAULT FORM

  $('.js-form').on('click', function () {

    $('.callback').fadeIn();

    $('.callback .callback__form form').removeClass('out');

    $('.callback .callback__form-out').addClass('out');

  });

  $('.callback .callback__back, .callback .callback__form-exit').on('click', function () {

    $('.callback').fadeOut();

    $('.callback__form form').addClass('out');

    $('.callback__form-out').removeClass('out');

  });

  $('.callback form input[type="submit"]').on('click', function (e) {

    e.preventDefault();

    $('.callback .callback__form form').addClass('out');

    $('.callback .callback__form-out').removeClass('out');

  });

      // ORDER TO CALL

  $('.manag-form').on('click', function () {

    $('.manager-callback').fadeIn();

    $('.manager-callback .callback__form form').removeClass('out');

    $('.manager-callback .callback__form-out').addClass('out');

  });

  $('.manager-callback .callback__back, .manager-callback .callback__form-exit').on('click', function () {

    $('.manager-callback').fadeOut();

    $('.manager-callback__form form').addClass('out');

    $('.manager-callback__form-out').removeClass('out');

  });

  $('.manager-callback form input[type="submit"]').on('click', function (e) {

    e.preventDefault();

    $('.manager-callback .callback__form form').addClass('out');

    $('.manager-callback .callback__form-out').removeClass('out');

  });



  // BUY PRODUCT

  var orgTitle = '';
  var orgPrice = '';

  $('.org__items-item__button').on('click', function () {

    $('.org-bye .callback__form form').removeClass('out');

    $('.org-bye .callback__form-out').addClass('out');

    orgTitle = $(this).closest('.org__items-item').find('.org__items-item__title').text();
    orgPrice = $(this).closest('.org__items-item').find('.org__items-item__price').text();

    $('.org-bye .org-bye__title').text(orgTitle);
    $('.org-bye .org-bye__price').text(orgPrice);

    $('.org-bye').fadeIn();
  });

  $('.org-bye .callback__back, .org-bye .callback__form-exit'). on('click', function () {

    $('.org-bye').fadeOut();

    $('.org-bye .org-bye__title').text('');
    $('.org-bye .org-bye__price').text('');
  });

  $('.org-bye form input[type="submit"]').on('click', function (e) {

    e.preventDefault();

    $('.org-bye .callback__form form').addClass('out');

    $('.org-bye .callback__form-out').removeClass('out');

  });


  $('.only_product__text-fast__buy').on('click', function () {

    $('.org-bye .callback__form form').removeClass('out');

    $('.org-bye .callback__form-out').addClass('out');

    orgTitle = $(this).closest('.only_product').find('.only_product__title').text();
    orgPrice = $(this).closest('.only_product').find('.only_product__text-price span').text();

    $('.org-bye .org-bye__title').text(orgTitle);
    $('.org-bye .org-bye__price').text(orgPrice);

    $('.org-bye').fadeIn();

  });

  //  FORM OFF

  //  LOCATION AND SEARCH ON

  $('.header-address__inner-cur').on('click', function () {

    $('.header-address__popup').fadeIn();

    $('.header-social__popup').fadeOut();

  });

  $('.header-address__popup-exit, .header-address__popup-back, .header-address__popup-list li').on('click', function () {

    $('.header-address__popup').fadeOut();

    $('.callback__form form').removeClass('out');

    $('.callback__form-out').addClass('out');

  });

  $('.header-social__search-link').on('click', function () {

    $('.header-social__popup').fadeIn();

    $('.header-address__popup').fadeOut();

  });

  $('.header-social__popup-exit').on('click', function () {

    $('.header-address__popup').fadeOut();

    $('.header-social__popup').fadeOut();

  });

  //  LOCATION AND SEARCH OFF

  //  ORG B/Y DETAIL FADE ON

  $('.org__items-item__char').on('click', function () {

    $(this).closest('.org__items-item').find('.org-detail').fadeToggle();

  });

  $('.org-detail__inner-exit, .org-detail__back').on('click', function () {

    $(this).closest('.org__items-item').find('.org-detail').fadeToggle();

  });

  //  ORG B/Y DETAIL FADE OFF

  //  MASKS ON

  $('input[type=tel]').mask('0(000) 000-0000');

  //  MASKS OFF

  // ORDER ON

  $('.cart-for-order .cart-bot__text-button a').on('click', function (e) {

    e.preventDefault();

    $('.thanks').fadeIn();

  });

  $('.thanks .callback__back, .thanks .callback__form-exit').on('click', function () {
    $('.thanks').fadeOut();
  });

  $('.order__block .order__block-button__link').on('click', function () {

    $(this).closest('.order__block').next().toggleClass('active');

    $(this).closest('.order__block').toggleClass('active');

  });

  $('.order__block-title span').on('click', function () {

    $('.order__block').removeClass('active');

    $(this).closest('.order__block').toggleClass('active');

  });

  // ORDER OFF

  // CART QUANTITY ON

  var cartCountProd = 1;
  var cartCountinput = $('.cart .cart-item__count input');

  function cartCount() {

    cartCountProd = $(this).val();

    if (cartCountProd < 1){
      $(this).val('1');
    }
  }

  cartCountinput.on('input', cartCount);
  cartCountinput.on('change', cartCount);

  // CART QUANTITY OFF

  // MAP POPUP ON

  $('.delivery__imgs-map').on('click', function () {

    $('.map-open').fadeIn();

  });

  $('.map-open .callback__back, .map-open .callback__form-exit').on('click', function () {

    $('.map-open').fadeOut();

  });

  // MAP POPUP OFF

  // ADD TO CART POPUP ON

  function MyAddToCart(e) {

    e.preventDefault();

    $('.add-to-cart-callback').fadeIn();

  }

  $('.c-items__item-button, .org__items-item__button, .only_product__text-to__cart').on('click', MyAddToCart );

  $('.add-to-cart-callback .callback__back, .add-to-cart-callback .callback__form-exit').on('click', function () {

    $('.add-to-cart-callback').fadeOut();

  });

  // ADD TO CART POPUP OFF

  $('#input-down-file').on('change', function () {

    $('#input-down-file .download__yep').text('Файл загружен');

  });

  $('.autsorting-filter__vision .ordered__button').on('click', function () {

    $('.autsorting-filter__vision').slideToggle();

    $('.autsorting-filter .form__title').text('Заполните контактные данные');

    $('.autsorting-filter__hidden').fadeToggle();

  });


  $('.catalogs__sort-off').on('click', function () {
    $('.catalogs__sort').toggleClass('active');
  });

});